function loadDoc() {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      var myArr=JSON.parse(this.responseText);
      myFunction(myArr);
      // console.log(myArr);
    }
  };
  xhttp.open("GET", "https://training.gemvietnam.com/dummy-api/users.json", true);
  xhttp.send();
}
function myFunction(data) {
  var i;
  var table="<thead> <tr>"
      +'<th scope="col">STT</th>'
      +'<th scope="col">First Name</th>'
      +'<th scope="col">Last Name</th>'
      +'<th scope="col">UserName</th>'
      +'<th scope="col">Email</th>'
      +'<th scope="col">Order</th>'
      +"</tr></thead>";
  for (i = 0; i <data.length; i++) { 
    table += "<tbody>"
    +"<tr>"
      +'<th scope="row">'+i+'</th>'
      +"<td>"+data[i].first_name+"</td>"
      +"<td>"+data[i].last_name+"</td>"
      +"<td>"+data[i].display_name+"</td>"
      +"<td>"+data[i].email+"</td>"
      +"<td>"+data[i].order+"</td>"

    +"</tr>"
    +"</tbody>";
  }
  document.getElementById("demo").innerHTML = table;
}

